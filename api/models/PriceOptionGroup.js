/**
* PriceOptionGroup.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    title:{
      type:'string',
      required:true
    },

    options:{
      collection:'PriceOptionItem',
      via:'group'
    },
    owner:{
      model:'User',
      required:true
    },

    isDefault:{
      type:'boolean',
      defaultsTo:false
    }



  }
};

