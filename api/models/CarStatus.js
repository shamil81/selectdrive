/**
* CarStatus.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    title:{
      type:'json'
    },
    description:{
      type:'json'
    },

    car:{
      collection:'Car',
      via:'status'
    },

    options:{
      type:'json'
    }

  }
};

