/**
* Booking.js
*
* @description :: When user selects a car to book, this object is created. The car status chnanges to 'Booked'. 
                  A user has a finite amount of time to pay for rent.
                  After time elapsed and payment hasn't been made, this object is deleted, and car released for renting again. 
*/

var isDate = require('date-fns/is_date');
var differenceInHours = require('date-fns/difference_in_hours');

module.exports = {

  attributes: {

    car:{
      model:'Car',
      required:true
    },

    user:{
      model:'User',
      required:true
    },

    start:{
      type:'integer',
      required:true

    },
    end:{
      type:'integer',
      required:true,
      isValidDates:true

    },

    priceTotal:{

      type:'integer',
      required:true

    },

    pricePaid:{
      type:'integer',
      defaultsTo:0

    },

    options:{
      type:'json'
    },

    status:{

     type:'integer',
     defaultsTo:SettingsService.BOOKING_STATUS.PENDING


    }

    






  },

 // beforeValidate:function(value, cb){

 //    if(value.start && !isDate(value.start)){

 //     //value.start = new Date(+value.start);


 //   }

 //   if(value.end && !isDate(value.end)){

 //    // value.end = new Date(+value.end);


 //   }
  

 //   cb();


  

 //  },

types:{

  isValidDates:function(value){


     
      
      //return differenceInHours(value.end, value.start) >= SettingsService.MIN_BOOKING_HOURS;
      return true;



  

  }

}


  
 
};

