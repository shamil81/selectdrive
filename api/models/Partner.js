/**
* Partner.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    user:{
      model:'user',
      required:true,
      unique:true

    },

    head:{
      type:'string',
      required:true

    },
    bank:{
      type:'string',
      required:true

    },
    bik:{
      type:'string',
      required:true,
      unique:true



    },
    //  Кор счет
    k_account:{
      type:'string',
      required:true,
      unique:true

    },
    // Расч счет
    r_account:{
      type:'string',
      required:true,
      unique:true

    }




  }
};

