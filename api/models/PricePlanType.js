/**
* PricePlanType.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    title:{
      type:'string',
      required:true,
      unique:true
    },

    pricePlan:{
      collection:'PricePlan',
      via:'pricePlanType'
    },
    //hour, date, month
    dataType:{
     type:'string',
     required:true
    },

    min:{
      type:'integer',
      defaultsTo:0
    },

    max:{
      type:'integer',
      defaultsTo:0

    }

  }
};

