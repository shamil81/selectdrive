/**
* CarClass.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    title:{
      type:'json'
    },
    image:{
      type:'string'
    },

    car:{
      collection:'Car',
      via:'carClass'
    },

    carModel:{
      collection:'CarModel',
      via:'carClass'
    }

  }
};

