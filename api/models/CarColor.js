/**
* CarColor.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    title:{
      type:'json'
    },
    hex:{
      type:'string',
      required:true,
      unique:true
    },
    car:{
      collection:'Car',
      via:'color'
    }

  }
};

