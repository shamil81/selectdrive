/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    auth:{
      model:'Auth'
    },


// number, date
    driverLicenseNumber:{
      type:'string',
      alphanumeric:true,
      unique:true
    },

    driverLicenseDate:{
      type:'date'
    },

    // serial, number, date, место выдачи  (partner must provide propiska)

    passportNumber:{
      type:'string',
      unique:true
    },

    passportDate:{
      type:'date'
    },

    passportIssuedBy:{
      type:'string'
    },

    firstName:{
      type:'string'
    },
    lastName:{
      type:'string'
    },
    middleName:{
      type:'string'
    },

    birthDate:{
      type:'date'
    },

    phone:{
      type:'string'
    },

    isLegalEntity:{
     type:'boolean',
     defaultsTo:false
    },

    // For storing additional data: name, inn, kpp, address
    legalEntityName:{
      type:'string'
    },

    legalEntityInn:{
      type:'string',
      numeric:true,
      unique:true
    },
    legalEntityKpp:{
      type:'string',
      alphanumeric: true,
      unique:true

    },

    address:{
      type:'string'
    },

    //End of legal entity

    // List of drivers for Legal entity
    drivers:{
      collection:'User',
      via:'createdBy',
      defaultsTo:null
    },

    createdBy:{
      model:'User',
      defaultsTo:null
    },


    locale:{
      type:'string'

    },
    confirmed:{

      type:'boolean',
      defaultsTo:false

    },

    partner:{
      model:'partner',
      defaultsTo:null,
        required:false
    },

    cars:{
      collection:'Car',
      via:'owner'
    },

    locations:{
      collection:'Location',
      via:'owner'
    },





    toJSON: function () {
      var obj = this.toObject();
      for(var i in obj){
        if(obj[i] === null){
          delete obj[i];
        }
      }
      return obj;
    }

  }


};

