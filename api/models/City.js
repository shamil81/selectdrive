/**
* City.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    title:{
      type:'json'
    },

    bounds:{
      type:'json'
    },

    location:{
      type:'json'
    },

    locations:{
      collection:'Location',
      via:'city'
    },
    utcOffset:{

      type:'integer',
      defaultsTo:3

    },

    isDisabled:{

      type:'boolean',
      defaultsTo:false

    },


    isDefault:{
      type:'boolean',
      defaultsTo:false
    }

  }
};

