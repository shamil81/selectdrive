/**
* CarImage.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var path = require('path');
module.exports = {

  attributes: {

    car:{
      model:'Car'
    },
// Web
    url:{
      type:'string'

    },

    // Local path
    path:{
      type:'string'
    },

    options:{
      type:'json'
    }

  },

  beforeCreate:function(values, cb){
    values.url = SettingsService.USER_UPLOAD_DIR + path.parse(values.path).base;
    cb();


  }
};

