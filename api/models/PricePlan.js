/**
* PricePlan.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    title:{
      type:'string',
      required:true
    },

    discount:{
      type:'integer',
      defaultsTo:0
    },

    pricePlanType:{

      model:'PricePlanType',
      required:true

    },


    price:{
      type:'json'

    },

    runLimit:{
      type:'integer',
      defaultsTo:200
    },


    overrun:{
      type:'integer',
      defaultsTo:0
    },



    owner:{
      model:'User',
      required:true
    },

    cars:{
      collection:'Car',
      via:'pricePlan'
    }

  }





};

