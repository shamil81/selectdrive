/**
* PriceOptionItem.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    priceOption:{
      model:'PriceOption',
      required:true
    },

    price:{
      type:'integer',
      required:true
    },

    group:{
      model:'PriceOptionGroup'
    }

  }
};

