/**
* Car.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    model:{
      model:'CarModel',
      required:true
    },

    images:{
      collection:'CarImage',
      via:'car'

    },

    carClass:{
      model:'CarClass'
    },

    carMaker:{
      model:'CarMaker'
    },

    body:{
      model:'CarBodyStyle'
    },


    color:{
      model:'CarColor',
      required:true

    },
    number:{
      type:'string',
      required:true,
      unique:true
    },
    description:{
      type:'json'
    },
    status:{
      model:'CarStatus'
    },
    isRight:{
      type:'boolean',
      defaultsTo:false
    },
// Transmission
    isAutomatic:{
      type:'boolean',
      defaultsTo:true
    },

    conditioning:{
      type:'boolean',
      defaultsTo:true
    },

    year:{
      type:'string',
      required:true

    },

    power:{
      type:'integer',
      required:true

    },

    consumption:{
      type:'integer'
    },
    fuel:{

      model:'FuelType',
      required:true



    },

    wheelDrive:{
      model:'Wheeldrive',
      required:true
    },

    doors:{
      type:'integer',
      defaultsTo:4
    },

    volume:{
     type:'integer',
     defaultsTo:1600
    },


    owner:{
      model:'User',
      required:true
    },

  //  pricePlan:{
  //     collection:'PricePlan',
  //     via:'cars',
  //     dominant:true
  //   },

    pricePlan:{
      model:'PricePlan'
    },
    rentConditions:{
      model:'RentConditionGroup'
    },

    priceOptionGroup:{
      model:'PriceOptionGroup'
    },


    location:{
      model:'Location'
    },

    customerRequirements:{
     model:'CustomerRequirement'

    },

    rating:{

      collection:'CarRating',
      via:'car'


    },

    bookings:{

      collection:'Booking',
      via:'car'


    }







  },
  beforeCreate:function (values,cb) {
    CarModel.findOne({id:values.model})
      .then(function (model) {
        values.carMaker = model.maker;
        values.carClass = model.carClass;
        values.body = model.body;

        cb();
      })
      .catch(function (err) {
        console.error(err);
      })
  },

  beforeUpdate:function (values,cb) {
   

    
    CarModel.findOne({id:values.model})
      .then(function (model) {
        if(!model){

      return cb();

    }
        values.carMaker = model.maker;
        values.carClass = model.carClass;
        values.body = model.body;
        cb();
      })
      .catch(function (err) {
        console.error(err);
      })
  }
};

