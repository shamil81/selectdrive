/**
* CarModel.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    title:{
      type:'string',
      required:true
    },


    image:{
      type:'string'
    },


    maker:{

      model:'CarMaker'

    },

    passengers:{

      type:'integer',
      defaultsTo:5


    },

    luggages:{

      type:'integer',
      defaultsTo:2


    },

    co2:{

      type:'integer',
      defaultsTo:105



    },



    body:{
      model:'CarBodyStyle'

    },


    carClass:{
      model:'CarClass',
      required:true
    },

    car:{
      collection:'Car',
      via:'model'
    },

    produceStart:{
      type:'integer',
      required:true
    },

    produceEnd:{
      type:'integer',
      required:true

    },

    media:{
      type:'json'
    }

  }
};

