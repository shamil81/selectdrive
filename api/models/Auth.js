/**
* Auth.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

      email:{
        type:'string',
        unique:true
      },
      password:{
        type:'string'
      },

      // Client = 0, Partner = 1
      userType:{
        type:'integer',
        defaultsTo:0
      },

      user:{
        model:'User'
      },



      toJSON: function () {
        var obj = this.toObject();
        delete obj.password;
        for(var i in obj){
          if(obj[i] === null){
            delete obj[i];
          }
        }
        return obj;
      }

  },
  beforeUpdate: function (values, next) {
    CipherService.hashPassword(values);
    next();
  },
  beforeCreate: function (values, next) {
    CipherService.hashPassword(values);
    next();
  }
};

