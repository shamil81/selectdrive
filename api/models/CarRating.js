/**
 * CarRating.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

  	car:{

  	  model:'Car',
      required:true
  	},

  	booking:{

  		model:'Booking',
  		required:true
  	},

  	stars:{
  		type:'integer',
  		defaultsTo:0,
  		min:SettingsService.MIN_RATING_STARS,
  		max:SettingsService.MAX_RATING_STARS
  	},

  	user:{
  		model:'User',
  		required:true
  	},

  	comment:{

  		type:'string'
  	}




  }
};

