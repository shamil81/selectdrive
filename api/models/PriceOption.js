/**
* PriceOption.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    title:{
      type:'json'
    },
    description:{
      type:'json'
    },

    image:{
      type:'string'

    },

    priceOptionItem:{
      collection:'PriceOptionItem',
      via:'priceOption'
    },

    // Makes some options available per city basis (e.g shipping to metro station). Submission narrowed to admin

    city:{
      model:'City'

    },

    createdBy:{
      model:'User'
    }

  }
};

