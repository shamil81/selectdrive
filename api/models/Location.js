/**
* Locations.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    // For partner use
    name:{
      type:'string'
    },
// Address ru/en
    title:{
      type:'json'
    },

    owner:{
      model:'User',
      required:true
    },

    lat:{
      type:'float',
      required:true
    },

    lng:{
      type:'float',
      required:true
    },
    cars:{
      collection:'Car',
      via:'location'
    },

    city:{
      model:'City'
    },
    isDefault:{
      type:'boolean',
      defaultsTo:false
    },

    businessHours:{
      type:'array',
      defaultsTo:[{"isActive":true,"timeFrom":"9:00","timeTill":"21:00"},{"isActive":true,"timeFrom":"9:00","timeTill":"21:00"},{"isActive":true,"timeFrom":"9:00","timeTill":"21:00"},{"isActive":true,"timeFrom":"9:00","timeTill":"21:00"},{"isActive":true,"timeFrom":"9:00","timeTill":"21:00"},{"isActive":true,"timeFrom":"9:00","timeTill":"21:00"},{"isActive":true,"timeFrom":"9:00","timeTill":"21:00"}]
    }

  },

  afterValidate:function(o, next){

    CommonService.handleDefaults('Location', o)
      .then(function(){
        return next();
      })
      .catch(function(err){
        sails.log.error(err);
      });


  }

};

