/**
* CustomerRequirements.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {


    attributes: {
      title:{
        type:'string',
        required:true
      },


      requirements:{
        type:'array'
      },

      owner:{
        model:'user'
      },

      cars:{
        collection:'Car',
        via:'customerRequirements'
      },
      isDefault:{
        type:'boolean',
        defaultsTo:false
      }

    },
  afterValidate:function(o, next){

    CommonService.handleDefaults('CustomerRequirement', o)
      .then(function(){
        return next();
      })
      .catch(function(err){
        sails.log.error(err);
      });


  }






};

