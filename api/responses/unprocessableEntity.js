/**
 * 422 (Unprocessable Entity) Handler
 *
 * Usage:
 * return res.unprocessableEnity();
 * return res.unprocessableEnity(data);
 *
 * 
 */

module.exports = function unprocessableEnity(data, options) {

  // Get access to `req`, `res`, & `sails`
  var req = this.req;
  var res = this.res;
  var sails = req._sails;

  // Set status code
  res.status(422);

  // Log error to console
  if (data !== undefined) {
    sails.log.verbose('Sending 422 ("Unprocessable Entity") response: \n',data);
  }
  else sails.log.verbose('Sending 422 ("Unprocessable Entity") response');

  // Only include errors in response if application environment
  // is not set to 'production'.  In production, we shouldn't
  // send back any identifying information about errors.
  if (sails.config.environment === 'production') {
    data = undefined;
  }

 
return res.json(data);

};

