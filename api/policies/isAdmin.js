/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 21/10/15
 */
var passport = require('passport');

module.exports = function (req, res, next) {
  passport.authenticate('jwt', function (error, user, info) {
    if (error) return res.serverError(error);
    console.log(user.id + ' name  ' + sails.config.admin.username);
    if (!user || user.id !== sails.config.admin.id){
      return res.unauthorized(null, info && info.code, info && info.message);
    }
    req.user = user;



    next();
  })(req, res);
};
