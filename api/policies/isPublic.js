/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 21/10/15
 *
 * This policy appends an user id if finds one
 */
var passport = require('passport');

module.exports = function (req, res, next) {
  passport.authenticate('jwt', function (error, user, info) {
    if (error) return res.serverError(error);
    if (user){
     return Auth.findOne({user:user.id})
        .then(function (result) {
          delete result.id;
          _.merge(user,result);
          req.user = user;

         return next();

        })
    }else{
      next();
    }


  })(req, res);
};
