/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 21/10/15
 */




module.exports = {

  /**
   * Generate random string
   */
  randStr:function(){
    return Math.random().toString(36).slice(2);

  }



};
