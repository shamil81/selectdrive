var Promise = require('bluebird');
var validator = require('validator');
var dateFns = require('date-fns');

module.exports = {

  adultAge: 18,


  // Properties that can not be updated by the user directly
  omit: ['confirmed', 'locations', 'isLegalEntity'],
  // Properties user can update without password
  allowed: ['locale'],

  isAdult: function (birthDate) {

    return Math.abs(dateFns.differenceInYears(new Date(birthDate), new Date())) >= this.adultAge;

  },

  // legalEntityProps: {
  //   legalEntityName: /^[A-Я]{1,}$/,
  //   legalEntityInn: /^[0-9]{10}$/,
  //   legalEntityKpp: /^[0-9]{10}$/,
  //   address: /^\w\s\-]{1,}$/,
  // },

  // nonLegalEntityProps: {
  //   driverLicenseNum: /^\w\s\-]{1,}$/,
  //   driverLicenseDate: validator.isAfter,
  //   passportNumber: /\d/,
  //   passportDate: UserService.isAdult,
  //   passportIssuedBy: /\w/,
  //   firstName: /^[A-zА-я\s]*$/,
  //   lastName: /^[A-zА-я\s]*$/,
  //   middleName: /^[\w\s]*$/,
  //   birthDate: UserService.isAdult,
  //   phone: validator.isMobilePhone,
  // },











  allowedUpdate: function (user) {


  },

  // TODO
  validate: function (user) {
    var errors = [];
    if (user.isLegalEntity) {

      //  _.forOwn(this.legalEntityProps, function(prop){

      //  })

      //  User.find({
      //    or:[
      //      legalEntityInn:user.legalEntityInn,
      //      legalEntityKpp:user.legalEntityKpp


      //    ]
      //  })




    }

    if (!validator.isEmail(user.email)) {
      errors.push('email');

    }


  },



  get: function (id) {

    return new Promise(function (resolve, reject) {
      User.findOne({ id: id })
        .populate('auth')
        .then(function (user) {
          user.email = user.auth.email;
          delete user.auth;
          return resolve(user);

        })
        .catch(function (err) {
          reject(err);
        })
    })

  },

  update: function (user) {

    return new Promise(function (resolve, reject) {
      Auth.findOne({ user: user.id })
        .then(function (auth) {


          user = _.omit(user, UserService.omit);



          delete user.auth;
          delete user.userType;



          console.dir(user);

          if (user.newPassword) {

            if (!CipherService.comparePassword(user.password, auth)) {
              return resolve('unauthorized');
            }

            UserService.updateCredentials(user, auth.email)
              .then(function (result) {
                if (result === true) {
                  sails.log.info('Email updated: new ' + user.email + ' old ' + auth.email);
                }

                console.log('res ' + result);


                if (result === 'conflict') {
                  return resolve('conflict');
                }




                User.update({ id: user.id }, user)
                  .then(function (usr) {
                    var o = {};
                    if (result.hasOwnProperty('token')) {
                      o.token = result.token;
                    }
                    usr[0].email = user.email;
                    o.user = usr[0];
                    return resolve(o);
                  })


              })
              .catch(function (err) {
                return reject(err);
              })
          } else {

            delete user.password;

            User.update({ id: user.id }, user)
              .then(function (results) {
                return resolve(results);
              })
              .catch(function (err) {
                return reject(err);
              })

          }






        })


    })
  },

  updateCredentials: function (user, email) {
    var payload = {};

    return new Promise(function (resolve, reject) {
      if (user.email !== email || (user.newPassword && user.newPassword.length >= SettingsService.MIN_PASS_LEN)) {
        payload = { email: user.email };
        if (user.newPassword && user.newPassword.length >= SettingsService.MIN_PASS_LEN) {
          user.password = user.newPassword;
          payload.password = user.newPassword;

        }

        Auth.findOne({ email: user.email, user: { '!': user.id } })
          .then(function (found) {
            if (found) {
              return resolve('conflict');
            }

            Auth.update({ user: user.id }, payload)
              .then(function () {
                payload = {};
                // Generate new token
                if (user.newPassword) {
                  payload.token = CipherService.createToken(user);
                }
                return resolve(payload);
              })
          })


          .catch(function (err) {
            return reject(err);
          })

      } else {

        resolve(false);

      }
    })

  },
  isPartner: function (user) {
    return (user && user.userType === SettingsService.USER_TYPE.PARTNER);
  }



}
