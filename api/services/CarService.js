/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 20/10/2016
 */
var Promise = require('bluebird');
var moment = require('moment');
var differenceInDays = require('date-fns/difference_in_calendar_days');
var differenceInHours = require('date-fns/difference_in_hours');

module.exports = {

    search: function(params) {

        return new Promise(function(resolve, reject) {

            // Query props lists
            var ar1 = ['skip', 'limit', 'lat', 'lng', 'city', 'start', 'end'],
                ar2 = ['carClass', 'color', 'carMaker', 'body', 'model', 'wheelDrive', 'fuel', 'isAutomatic', 'maxYear', 'minYear', 'maxVol', 'minVol'],
                allowed = ar1.concat(ar2),
                got = Object.keys(params),
                diffs = _.difference(got, allowed);


            // Validate request 

            if (diffs.length > 0) {

                return reject({
                    status: 400,
                    error: 'Bad field(s): ' + diffs.join(diffs, ',')
                });

            }






            var skip = parseInt(params.skip),
                limit = parseInt(params.limit),
                isWithinBounds,
                q = _.omit(params, ar1),
                o3 = _.pick(params, ar2),
                dbQ = {},
            filteredLocations = [];
 


            if (Object.keys(o3).length > 0) {

                if (o3.maxYear && o3.minYear && o3.maxYear === o3.minYear) {

                    o3.year = o3.maxYear;

                    delete o3.maxYear;
                    delete o3.minYear;

                }

                if(o3.minVol && o3.maxVol && o3.minVol === o3.maxVol){

                    o3.volume = o3.maxVol;

                    delete o3.maxVol;
                    delete o3.minVol;



                }


                _.forOwn(o3, function(v, k) {



                    var num = parseInt(v);
                    if (isNaN(num)) {

                        if (k === 'isAutomatic') {
                            num = (v === 'true');
                        } else {
                            return reject({
                                status: 400,
                                error: 'Bad field(s): ' + k
                            });

                        }

                    }

                    switch (k) {
                        case 'maxYear':

                            k = 'year';
                            num = {
                                '<=': num
                            };

                            break;
                        case 'minYear':
                            k = 'year';
                            num = {
                                '>=': num
                            };
                            break;
                        case 'maxVol':
                            k = 'volume';
                            num = {
                                '<=': num
                            };
                            break;
                        case 'minVol':
                            k = 'volume';
                            num = {
                                '>=': num
                            };



                    }




                    dbQ[k] = num;




                });


            }
            console.dir(dbQ);



            dbQ.status = [SettingsService.CAR_STATUS.AVAILABLE, SettingsService.CAR_STATUS.RELEASE];

            params.location = {
                lat: params.lat,
                lng: params.lng
            };


            SearchService.getCity(params.city)
                .then(function(city) {


                    // Check whether supplied coordinates are within city

                    isWithinBounds = LocationService.withinBounds(city, params.location);

                    // if(!isWithinBounds){

                    //   return reject({status:422, error:'Coordinates ' + params.location.lat + ' ' + params.location.lng + ' are out of bounds of ' + city.title.en });


                    // }

                    Location.find({
                            city: city.id
                        })
                        .populate('cars', dbQ)
                        .populate('cars.model.maker')
                        .populate('cars.color')
                        .populate('cars.carClass')
                        .populate('cars.body')
                        .populate('cars.pricePlan')
                        .populate('cars.wheelDrive')
                        .populate('cars.fuel')
                        .populate('cars.images')
                        .populate('cars.rating')
                        .populate('cars.bookings', {
                            status: [SettingsService.BOOKING_STATUS.PENDING, SettingsService.BOOKING_STATUS.ACTIVE]
                        })
                        .then(function(locations) {
                            locations.forEach(function(location, i) {
                                if (location.cars && location.cars.length > 0) {

                                    location.distance = LocationService.getDistance(location, params.location);

                                    if (location.cars.length > 0) {
                                        filteredLocations.push(location);

                                    }



                                }
                            });




                            var results,
                                cars = [];


                            if (isWithinBounds) {
                                results = _.sortBy(filteredLocations, function(location) {
                                    return location.distance;
                                });
                            } else {
                                results = filteredLocations;
                            }

                            results.forEach(function(location) {

                                for (var i = 0; i < location.cars.length; i++) {

                                    var car = location.cars[i];



                                    var pendingBookings = car.bookings.filter(function(b) {


                                        return b.status === SettingsService.BOOKING_STATUS.PENDING;

                                    });


                                    // Remove car with overlapping bookings
                                    if (BookingService.isBooked({
                                            start: params.start,
                                            end: params.end,
                                            bookings: pendingBookings
                                        })) {
                                        continue;
                                    }

                                    // Get remaining time for release

                                   car.release = BookingService.getRemainHours(car);


                                    delete car.bookings;


                                    car.price = CarService.getPrice(car, params.start, params.end);

                                    delete car.pricePlan;

                                    cars.push(car);


                                }


                            });


                            if (!isNaN(skip) && skip > 0) {
                                cars.splice(0, skip);
                            }

                            if (!isNaN(limit) && limit > 0) {
                                cars.splice(limit);
                            }

                            return resolve(cars);
                        })
                })
                .catch(reject);

        });


    },

    getPrice: function(car, start, end) {




        var prices = car.pricePlan.price,

           duration = moment.duration(moment(new Date(+end)).diff(moment(new Date(+start)))),
            days = duration.days(),
            hours = duration.hours(),

            planDiscount = Math.abs(car.pricePlan.discount * .01),


            // Get available days/price
            priceList = Object.keys(prices),

            priceDay = CarService.closestNum(days, priceList),

            priceObj = prices[priceDay],

            discount = Math.abs(priceObj.discount * .01);





        var limitedPrice = CarService.calcPriceHour(priceObj.limited, days, hours);




        var unlimitedPrice = CarService.calcPriceHour(priceObj.unlimited, days, hours);


        var limited = limitedPrice - limitedPrice * discount - limitedPrice * planDiscount;


        var unlimited = unlimitedPrice - unlimitedPrice * discount - unlimitedPrice * planDiscount;



        var price = {
            runLimit: car.pricePlan.runLimit,
            limited: limited,
            unlimited: unlimited,
            day: {
                limited: priceObj.limited,
                unlimited: priceObj.unlimited
            },
            discount: priceObj.discount,
            planDiscount: planDiscount,
            currency: 'RUR'


        };




        return price;



    },

    calcPriceHour: function(price, days, hours) {

        var total = price * days;


        if (hours > 9) {
            total += price;

        } else {
            var perHour = price * 10 * .01;

            
            total = perHour * hours + total;
            
        }

       // console.log(total);

        return total;



    },

    closestNum: function(num, ar) {
        var closest = parseInt(ar[0], 10),
            diff = Math.abs(num - closest);
        ar.forEach(function(v) {
            var val = parseInt(v, 10),
                newDiff = Math.abs(num - val);

            if (newDiff < diff) {
                diff = newDiff;
                closest = val;
            }
        });

        return closest;

    },

    carById: function(id) {

        return new Promise(function(resolve, reject) {

            Car.find({
                    id: id
                })
                .populate('model.maker')
                .populate('color')
                .populate('carClass')
                .populate('body')
                .populate('pricePlan')
                // .populate('priceOptionGroup')
                .populate('wheelDrive')
                .populate('fuel')
                .populate('images')
                .then(resolve)
                .catch(reject);

        })




    },


    getCar: function(id, bookingStatus) {

        return new Promise(function(resolve, reject) {

            var options = bookingStatus || {};



            Car.findOne({
                    id: id,
                    status: [SettingsService.CAR_STATUS.AVAILABLE, SettingsService.CAR_STATUS.RELEASE]
                })
                .populate('model.maker')
                .populate('color')
                .populate('carClass')
                .populate('body')
                .populate('pricePlan')
                .populate('priceOptionGroup.options.priceOption')
                .populate('wheelDrive')
                .populate('fuel')
                .populate('images')
                .populate('location.city')
                .populate('bookings', options)
                .then(resolve)
                .catch(reject);

        })



    }


};