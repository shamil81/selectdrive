/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 08/01/16
 */
module.exports = {
  calculateAspectRatioFit: function(srcWidth, srcHeight, maxWidth, maxHeight) {

  var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

  return { width: srcWidth*ratio, height: srcHeight*ratio };
}


}
