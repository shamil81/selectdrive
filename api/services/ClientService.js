/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 21/10/15
 */


var validator = require('is-my-json-valid');

module.exports = {


  /**
   * Validate regular client
   */
  validateRegular:function(payload){

    var passRegEx = new RegExp("^[a-zA-Z@_#$&*()0-9]{" + SettingsService.MIN_PASS_LEN + ",}$");

    var validateCommon = validator({
        title: "Client",
        type: "object",
        required: true,
        properties: {
          firstName: {
            type: "string",
            required: true
          },
          lastName: {
            type: "string",
            required: true
          },
          middleName: {
            type: "string"
          },
          email: {
            type: "string",
            format: "email",
            required:true
          },
          password:{
            type:"string",
            format:"password",
            required:true
          }


        }
      },
      {
        formats:{
          "email":/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
          "password":passRegEx

        },
        verbose:true,
        greedy:true
      }

    );


    var driverLicense = validate({
      title: "DriverLicense",
      type: "object",
      required: true,
      properties:{
        serial:{
          type:"string",
          required:true
          //format:"Serial"
        },
        number:{
          type:"number",
          reuired:true
         // format:"Number"
        }
      }


    },{
      formats:{


      },
      verbose:true
    })



    var isValid = validate(payload);

    return {valid:isValid, errors:validate.errors};







  },
  /**
   * Register Legal entity client
   */
  registerLegalEntity:function(){

  },
  /**
   * Add driver (by legal entity)
   */
  registerByLegalEntity:function(){

  }



};
