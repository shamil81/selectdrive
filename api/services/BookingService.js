var Promise = require('bluebird');
var moment = require('moment');
//const MomentRange = require('moment-range');
//const moment = MomentRange.extendMoment(Moment);
var differenceInDays = require('date-fns/difference_in_days');
var differenceInHours = require('date-fns/difference_in_hours')
var subMinutes = require('date-fns/sub_minutes');
var areRangesOverlapping = require('date-fns/are_ranges_overlapping');
var isEqual = require('date-fns/is_equal');
var addHours = require('date-fns/add_hours');
var addMinutes = require('date-fns/add_minutes');
var format = require('date-fns/format');
var subHours = require('date-fns/sub_hours');

var schedule = require('node-schedule');

module.exports = {

    /**
     * Pending bookings BookingService.session[userId]
     * @prop {pointer} timer - Ref to timer
     * @prop {object} car - Car
     * @prop {object} booking - Booking
     */

    session: {},

    /**
     * Milliseconds for booking pay
     */
    delay: function() {
        return SettingsService.BOOKING_TIME_MINUTES * 1000 * 60
    },


    /**
     * Validates car and dates
     * @param {integer} car - car id
     * @param {timestamp} start - start time
     * @param {timestamp} end - end time
     * @return {object | boolean} - Either car object or false in case of validation error
     */


    isCarAvailable: function(p) {


        return new Promise(function(resolve, reject) {
            //console.log(p.car);

            CarService.getCar(p.car, {
                    status: SettingsService.BOOKING_STATUS.PENDING
                })
                .then(function(car) {

                    if (!car) {
                        return reject({
                            status: 404
                        });
                    }




                    var found = BookingService.isBooked({bookings:car.bookings, start:p.start, end:p.end});
                    if (found) {


                        return reject({
                            status: 404
                        });

                    }


                    return resolve(car);




                })



        });



    },

    /**
     * Whether car is already booked within provided dates
     * @param {timestamp} start - start time
     * @param {timestamp} end - end time
     * @param {array} bookings - Pending bookings
     * @returns {boolean | integer} - Either false or id of overlapping booking
     */


    isBooked: function(p) {


        var idx,
            b = p.bookings || [];


        function overlaps(booking, i, ar) {
            idx = i;
            return areRangesOverlapping(new Date(+p.start), new Date(+p.end), new Date(booking.start), new Date(booking.end));

        }





        return b.some(overlaps) ? b[idx] : false;

    },

    /**
     * Get car and calculate booking price
     * @param {integer} car - car id
     * @param {date} start 
     * @param {date} end
     * @return {object | boolean} - Either car or false if car is not available
     * 
     */

    getBookingCar: function(p) {

        return new Promise(function(resolve, reject) {

            BookingService.isCarAvailable(p)
                .then(function(car) {

                    var price = CarService.getPrice(car, p.start, p.end);

                    var priceoptions = [];


                    if (car.priceOptionGroup) {

                        priceoptions = car.priceOptionGroup.options.map(function(opt) {

                            return {
                                id: opt.id,
                                price: opt.price,
                                title: opt.priceOption.title
                            };

                        });

                        delete car.priceOptionGroup;

                    }

                    car.release = BookingService.getRemainHours(car);

                    return resolve({
                        car: car,
                        price: price,
                        priceoptions: priceoptions
                    });



                })
                .catch(reject);




        });


    },


    makeIntent: function(p) {



        return new Promise(function(resolve, reject) {

            if (!BookingService.validate(p)) {

                console.log('mkInt validate dates');

                return reject({
                    status: 422,
                    data:'Bad days'
                });


            }

             var s = BookingService.session[p.user];

        if(!s) {

             BookingService.session[p.user] = {};

        }else if(Object.keys(s).length === 0){

            // Concurrent bookings from a single user

            return reject({status:422});
            
        }


            




            if (s && s.car.id == p.car) {

                // Maybe dates have changed
                if (isEqual(+p.start, s.booking.start) && isEqual(+p.end, s.booking.end)) {

                    return resolve({
                        id: s.booking.id,
                        car: s.car,
                        price: s.price,
                        priceoptions: s.priceoptions
                    });


                }
                // A new rent dates detected
                else {

                    BookingService.cancel(p)
                        .then(function() {


                            BookingService.createPendingBooking(p)
                                .then(resolve)
                                .catch(reject);


                        })
                        .catch(reject);




                }



            } else if (s && s.car.id !== p.car) {


                BookingService.cancel(p)
                    .then(function() {


                        BookingService.createPendingBooking(p)
                            .then(resolve)
                            .catch(reject);


                    })
                    .catch(reject);

            } else {

                BookingService.createPendingBooking(p)
                    .then(resolve)
                    .catch(reject);


            }




        });


    },

    createPendingBooking: function(p) {





        return new Promise(function(resolve, reject) {

            if (!BookingService.validate(p)) {
                return reject({
                    status: 422
                });

            }



            p.status = SettingsService.BOOKING_STATUS.PENDING;

            BookingService.getBookingCar(p)
                .then(function(o) {

                    var car = o.car;
                    var price = o.price;
                    var priceoptions = o.priceoptions;
                    p.priceTotal = price.limited;

                    



                    Booking.create(p)
                        .then(function(booking) {

                            var cached = {

                                booking: booking,
                                car: car,
                                price: price,
                                priceoptions: priceoptions


                            };

                            cached.timer = setTimeout(BookingService.cancel, BookingService.delay(), {
                                id: booking.id,
                                user: p.user
                            });

                            BookingService.session[p.user] = cached;

                            return resolve({
                                id: booking.id,
                                car: car,
                                price: price,
                                priceoptions: priceoptions
                            });



                        })
                        .catch(reject);




                })
                .catch(reject);




        });


    },

    /**
     * Process booking step 2
     * @param {integer} id - Booking id
     * @param {array} priceoptions
     */

    makeBooking: function(p) {


        return new Promise(function(resolve, reject) {

            var s = BookingService.session[p.user];

            if (!s) {

                return reject({
                    status: 404
                });


            }

            var carId = s.car.id;
            var booking = s.booking;
            BookingService.clearSession(p.user);

            BookingService.calcOptions({priceoptions:p.priceoptions, car:s.car})
            .then(function(optionsPrice){


                var price = CarService.getPrice(s.car, s.booking.start, s.booking.end).limited + optionsPrice;


                Booking.update({id:p.id}, {priceTotal:price})
                .then(function(bkng){

                     Car.findOne({
                    id: carId
                })
                .populate('owner.auth')
                .populate('model')
                .populate('carMaker')
                .then(function(car) {

                    console.dir(car);




                    User.findOne({
                            id: p.user
                        })
                        .populate('auth')



                        .then(function(user) {

                            var m = moment;
                            console.dir(m);

                            m.locale('ru');
                           // console.log(m.locale());
                            var start = m(new Date(booking.start)).format('LLLL');
                            var end = m(new Date(booking.end)).format('LLLL');

                            var msgParams = {
                                email: car.owner.auth.email,
                                car: car,
                                url: {
                                    allow: SettingsService.PARTNER_URL + 'booking?allow=' + booking.id,
                                    deny: SettingsService.PARTNER_URL + 'booking?deny=' + booking.id
                                },
                                user: user,
                                start:start,
                                end:end

                            };



                            MailService.partnerBookingConfirmation(msgParams.email, msgParams, function(result) {
                                if (result.error) {
                                    return reject(result.error);


                                }

                                MailService.clientBookingConfirmation(user.auth.email, {
                                    locale: user.locale
                                }, function(result) {

                                    if (result.error) {

                                        return reject(result.error);


                                    }




                                    var d = addHours(new Date(), SettingsService.BOOKING_CONFIRMATION_DELAY_HOURS);
                                    var partnerConfirmationTimer = schedule.scheduleJob(d, function(partnerConfirmationTimer) {

                                        console.log('confirmation check: Booking id: ' + booking.id);




                                        Booking.findOne({
                                                id: booking.id
                                            })
                                            .then(function(b) {

                                                if (b.status == SettingsService.BOOKING_STATUS.PENDING) {

                                                    var bookingTimer = schedule.scheduleJob(new Date(b.start), function() {

                                                        console.log('schedule check');
                                                        console.dir(b);


                                                        Car.update({
                                                                id: b.car
                                                            }, {
                                                                status: SettingsService.CAR_STATUS.BOOKED
                                                            })
                                                            .then(function() {
                                                                console.dir(b);

                                                                Booking.update({
                                                                        id: b.id
                                                                    }, {
                                                                        status: SettingsService.BOOKING_STATUS.ACTIVE
                                                                    })
                                                                    .then(function(b2) {
                                                                        b2 = b2[0];

                                                                        var releaseDate = subHours(new Date(b2.end), SettingsService.RELEASE_HOUR);


                                                                        var releaseBooking = schedule.scheduleJob(releaseDate, function() {

                                                                            console.log('schedules release');
                                                                            console.dir(b2);




                                                                            Car.update({
                                                                                    id: b2.car
                                                                                }, {
                                                                                    status: SettingsService.CAR_STATUS.RELEASE
                                                                                })
                                                                                .then(function() {

                                                                                    var endBookingSchedule = schedule.scheduleJob(new Date(b2.end), function() {

                                                                                        console.log('schedule end');


                                                                                        Car.update({
                                                                                                id: b2.car
                                                                                            }, {
                                                                                                status: SettingsService.CAR_STATUS.AVAILABLE
                                                                                            })
                                                                                            .then(function() {

                                                                                                Booking.update({
                                                                                                        id: b2.id
                                                                                                    }, {
                                                                                                        status: SettingsService.BOOKING_STATUS.COMPLETE
                                                                                                    })
                                                                                                    .then(function() {

                                                                                                        console.log('booking complete');


                                                                                                    })
                                                                                                    .catch(console.error);




                                                                                            })


                                                                                    }.bind(null, b2));


                                                                                })
                                                                                .catch(console.error);




                                                                        }.bind(null, b2));




                                                                    })


                                                            })
                                                            .catch(console.error);

                                                    }.bind(null, b));


                                                }


                                            })
                                            .catch(function(err) {

                                                console.error(err);


                                            });




                                    }.bind(null, booking));




                                    return resolve(result.ok);




                                });



                            });




                        })

                        .catch(reject);




                });


                })
                .catch(function(err){

                Booking.update({id:p.id}, {status:SettingsService.BOOKING_STATUS.CANCELED})
                .then(function(){

                    reject(err);


                });
                
                    
                    
                });


            })
            .catch(function(err){

                Booking.update({id:p.id}, {status:SettingsService.BOOKING_STATUS.CANCELED})
                .then(function(){

                    reject(err);


                });
                
                
                
            });




           




        });



    },




    /**
     * Calc options price
     * @param {array} priceoptions - Price options
     * @param {object} car - A Car object being rented
     */

    calcOptions: function(params) {
        return new Promise(function(resolve, reject) {
            console.log('priceoptions: ');
            console.dir(params.priceoptions);

            PriceOptionItem.find({
                    id: params.priceoptions
                })
                .populate('priceOption')
                .then(function(opts) {

                    var price = 0;

                    // Check whether price option is created by the car owner

                    for (var i = 0; i < opts.length; i++) {

                        if (opts[i].priceOption.createdBy != SettingsService.ADMIN.ID && opts[i].priceOption.createdBy != params.car.owner) {

                            return reject({
                                status: 422,
                                data:'Bad options'
                            });

                        }

                        price += opts[i].price;

                    }


                    return resolve(price);

                })
                .catch(reject);




        });

    },

    /**
     * Booking date validation
     * checks whether end time is ahead of start time
     * @param {date} start - Booking start time
     * @param {date} end - Booking end time
     * @return {boolean} - true - if dates are valid
     *
     */

    validate: function(p) {

        var d = new Date(),
            start = new Date(+p.start),
            end = new Date(+p.end);

            // console.log(start, end);
            // console.log(differenceInHours(start, d));
            // console.log(differenceInHours(start, d) >= SettingsService.MIN_HOURS_BEFORE_BOOKING);


        return differenceInHours(start, d) >= SettingsService.MIN_HOURS_BEFORE_BOOKING ||
            differenceInHours(end, start) < SettingsService.MIN_BOOKING_HOURS ||
            differenceInHours(end, start) > SettingsService.MAX_BOOKING_HOURS;




    },




    /**
     * Cancel booking - if no params passed
     * cancel current session booking
     * otherwise cancel by id
     * @param {integer} user - User id
     * @param {integer} [id] - Booking id
     * @return {integer} id - Car id
     */


    cancel: function(p) {

        return new Promise(function(resolve, reject) {

            var id = p.id,
                session = BookingService.session[p.user];


            if (!session && !id) {
                return reject({
                    status: 404,
                    data: 'No active bookings'
                });
            }


            if (session && !id || session && session.booking == id) {

                id = session.booking.id;
                BookingService.clearSession(p.user);


            }



            Booking.update({
                    id: id,
                    user: p.user
                }, {
                    status: SettingsService.BOOKING_STATUS.CANCELED
                })
                .then(function(bookings) {

                    resolve(bookings[0].id);

                })
                .catch(reject)




        });
    },
    /**
     * Clear timer and remove pending booking from cache
     * @param id - User id
     */
    clearSession: function(id) {

        var cache = BookingService.session[id];

        if (!cache) {

            return;
        }


        clearTimeout(cache.timer);

        delete BookingService.session[id];



    },



    /**
     * Booking settings
     * 1. Minimal start datetime
     * a. Current time till 18:00 - then now + MIN_HOURS_BEFORE_BOOKING
     * b. Current time past 18:00 - then tomorrow 12
     * 2. Minimal end - start + minBookingHours hour
     * @param {integer} city - city id
     * @return {object} minStart, minBookingHours, maxBookingHours
     */


    getSettings: function(id) {

        return new Promise(function(resolve, reject) {

            City.findOne({
                    id: id
                })
                .then(function(city) {

                    if (!city) {

                        return reject({
                            status: 404
                        });
                    }

                    var round = 15;


                    var minStart = BookingService.toUtc(city.utcOffset);
                    var remainder = round - minStart.minute() % round;
                    minStart.add(remainder, 'm');
                   // var minStart = moment();

                   console.log('min ' + minStart.hour());

                    minStart.hour() < 18 ? minStart.add(SettingsService.MIN_HOURS_BEFORE_BOOKING, 'h') : minStart.add(1, 'd').hour(12).minute(0);

                    resolve({
                        minStart: minStart.valueOf(),
                        minBookingHours: SettingsService.MIN_BOOKING_HOURS,
                        maxBookingHours: SettingsService.MAX_BOOKING_HOURS(),
                        utcOffset: city.utcOffset
                    });


                })
                .catch(reject);


        });




    },

    toUtc: function(offset) {


        var m = moment().clone();
        m.utc();
        m.utcOffset(offset);

        return m.clone();




    },

    getRemainHours: function(car) {

        var remains,
        now = new Date();

        if (car.status === SettingsService.CAR_STATUS.RELEASE) {

            car.bookings.forEach(function(b) {

                if (b.status = SettingsService.BOOKING_STATUS.ACTIVE) {

                    remains = differenceInHours(new Date(b.end), now);


                }


            });




        }


        return remains;



    }




};