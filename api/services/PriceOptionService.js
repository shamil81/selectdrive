var Promise = require('bluebird');

module.exports = {
  updateGroup:function(data){

    return new Promise(function(resolve,reject){
      console.dir(data);
      PriceOptionGroup.update({id:data.id}, data)
        .then(function(groups){
          PriceOptionGroup.findOne({id:groups[0].id})
            .populate('options')
            .then(function(group){
              resolve(group);

            });
        })
        .catch(function(err){
          reject(err);
        });
    })

  }
};
