/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 03/12/15
 */

var Promise = require('bluebird'),
  request = require('request');
module.exports = {

  process: function (location, user) {

    return new Promise(function (resolve, reject) {

      location.owner = user;

      // Geocode location
      var params = {
          latlng: location.lat + ',' + location.lng,
          language: 'ru'
        },
        url = 'http://maps.googleapis.com/maps/api/geocode/json',
        city = null,
        req = Promise.promisify(request),
        area,
        areaEn;


      Promise.all([
        req({url: url, qs: params, json: true}),
        req({url: url, qs: _.merge(params, {language: 'en'}), json: true})
      ])
        .then(function (ar) {


          area = LocationService.getArea(ar[0].body.results);
          areaEn = LocationService.getArea(ar[1].body.results);


          city = {
            title: {
              ru: (area && area.address_components[0].long_name) ? area.address_components[0].long_name : null,
              en: (areaEn && areaEn.address_components[0].long_name) ? areaEn.address_components[0].long_name : null
            }
          };

          if (city.title.ru === null || city.title.en === null) {
            return Promise.reject({error: 'Не определить местоположение'});
          }

          location.title = {
            ru: ar[0].body.results[0].formatted_address,
            en: ar[1].body.results[0].formatted_address
          };

          return ar[0].body.results;
        })
        .then(function (data) {


          City.find()

            .then(function (cities) {

              var found = cities.filter(function (c) {
                return c.title.ru.localeCompare(city.title.ru) === 0;
              })[0];

              if (found) {

                location.city = found.id;


                resolve(LocationService.query(location));

              } else {

                
                city.location = area.geometry.location;
                city.bounds = area.geometry.bounds;

                var timeStamp = (Date.now() || new Date().getTime()) * 0.001,

                timeZoneApiRequestUrl = "https://maps.googleapis.com/maps/api/timezone/json?location=" +
                            city.location.lat + "," + city.location.lng +
                            "&timestamp=" + timeStamp;

               Promise.all([req({url:'https://maps.googleapis.com/maps/api/timezone/json', qs:{location: city.location.lat + "," + city.location.lng, timestamp:timeStamp,json:true}})])
               .then(function(results){

                 //console.dir(results);

                 var data = JSON.parse(results[0].toJSON().body);


                 city.timezone = (data.rawOffset + data.dstOffset || 0) / 3600;



                  City.create(city)

                  .then(function (city) {

                    location.city = city.id;

                    resolve(LocationService.query(location));

                  })
                  .catch(reject);


               })
               .catch(reject);
                                 
                                   

               
              }
            })

        }, function (err) {
          reject(err);
        })
        .catch(function (err) {
          reject(err);
        });


    });

  },

  // Is location within city bounds
  withinBounds:function (city, location) {
    if(!location){
      return false;
    }
    var b = {};
    b.ne = city.bounds.northeast;
    b.sw = city.bounds.southwest;

    if(location.lat <= b.ne.lat && location.lng <= b.ne.lng && location.lat >= b.sw.lat && location.lng >= b.sw.lng){
      return true;
    }

    return false;


  },


  getArea: function (results) {

    return results.filter(function (o) {
      return o.types.indexOf("locality") !== -1;
    })[0];

  },

  query: function (location) {

    if (location.hasOwnProperty('id')) {
      return Location.update({id: location.id}, location);
    }
    return Location.create(location);
  },

  getDistance: function (loc1, loc2) {
    var R = 6371, // Radius of the earth in km
      dLat = (loc2.lat - loc1.lat) * Math.PI / 180,
      dLon = (loc2.lng - loc1.lng) * Math.PI / 180,
      a = 0.5 - Math.cos(dLat) * 0.5 + Math.cos(loc1.lat * Math.PI / 180) * Math.cos(loc2.lat * Math.PI / 180) * (1 - Math.cos(dLon)) * 0.5;

    return R * 2 * Math.asin(Math.sqrt(a));

  }
}
