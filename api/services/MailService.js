var nodemailer = require('nodemailer'),
  sendmailTransport = require('nodemailer-sendmail-transport'),
  EmailTemplate = require('email-templates').EmailTemplate,
  Promise = require('bluebird'),
  //wellknown = require('nodemailer-wellknown'),


/*smtpTransport = require('nodemailer-smtp-transport'),
transporter = nodemailer.createTransport(smtpTransport({

  host:'vmi49720.contabo.host',
  debug:true,
  from: 'info@vmi49720.contabo.host',
  tls: {
    rejectUnauthorized: false
  }
})),*/
    /*smtpConfig = {
      host: 'smtp.yandex.ru',
      port: 465,
      from:'info@selectdrive.ru',
      secure: true, // use SSL
      auth: {
        user: 'selectdrive@yandex.ru',
        pass: 'RFq98=YT'
      }
    },*/

    smtpConfig = {
      service:"Yandex",
      /*auth: {
        user: 'selectdrive@yandex.ru',
        pass: 'RFq98=YT'
      }*/
      auth:{
        user:'donotreply@selectdrive.ru',
        pass:'123456'
      }
    },

  handlebars = require('handlebars'),

async = require('async'),
//  transporter = nodemailer.createTransport(sendmailTransport()),
   transporter = nodemailer.createTransport(smtpConfig),
  path = require('path'),
  partnerRegistration = path.join(__dirname, '..','..','templates','partnerregistration'),
  partnerReminder = path.join(__dirname, '..','..','templates','partnerreminder');
  partnerbooking = path.join(__dirname, '..','..','templates','partnerbooking');
  clientbooking = path.join(__dirname, '..','..','templates','clientbooking');


 // userRegistration = new EmailTemplate(path.join(templateDir, 'user-registration'));



module.exports = {
  compiled:false,

  compile:function(email,url){


  },
  testMail:function(){
    transporter.sendMail({
      from: 'donotreply@selectdrive.ru',
      to:'shamil.bikbov@gmail.com',
      subject:'Test email',
      html:'<h1>Let\'s have some test'
    },function (error, response) {
      if (error) {
        console.dir(error);
      } else {
        console.log('message sent');
      }
    });

  },

  sendActivation:function(email,url,locale,cb) {
    console.log(locale + ' ' + url);
    transporter.sendMail({
      from:'donotreply@selectdrive.ru',
      to: email,
      subject: sails.__({phrase:"mail-activation-title", locale:locale}),
      html: '<body style="color:#282828;height:500px;font-family: Roboto, Verdana,sans-serif"><h1>'+ sails.__({phrase:"mail-activation-title", locale:locale}) +'</h1><p style="background-color:#fde758";height:300px;>' + sails.__({phrase:"mail-activation-body",locale:locale}, sails.config.url.client + "/confirm/?key=" + url) + '</p></body>'

    }, function (error, response) {
      if (error) {
        console.dir(error);
        cb({error:error});
      } else {
        cb({ok:'Message sent'});
      }

    })
  },


  sendReminder:function (email, hash) {
    return new Promise(function(resolve, reject){



      handlebars.registerPartial('link', '{{link.href}}');
      console.log(partnerRegistration);

      var template = new EmailTemplate(partnerReminder),
        locals = {link:{href:SettingsService.PARTNER_URL+'confirm/'+hash}};

      template.render(locals)
        .then(function(results){
          sails.log.info(results);

          transporter.sendMail({from:'donotreply@selectdrive.ru', to:email, subject:'Сброс пароля selectdrive.ru', html:results.html}, function(err,res){
            if(err){
              return reject(err);
            }
            else{
             return resolve({ok:'Message sent'});
            }
          })
        })



    });
  },

  partnerBookingConfirmation:function(email, params, cb){

   //handlebars.registerPartial('link', '{{link.href}}');
    console.log(partnerRegistration);

    var template = new EmailTemplate(partnerbooking);
     

    template.render(params)
      .then(function(results){
        sails.log.info(results);

        transporter.sendMail({from:'donotreply@selectdrive.ru', to:email, subject:'Новое бронирование', html:results.html}, function(err,res){
          if(err){
            return cb({error:err});
          }
          else{
            cb({ok:'Message sent'});
          }
        })
      })





  },

  clientBookingConfirmation:function(email, params, cb){

   //handlebars.registerPartial('link', '{{link.href}}');
    console.log(partnerRegistration);

    var template = new EmailTemplate(clientbooking);
     

    template.render(params)
      .then(function(results){
        sails.log.info(results);

        transporter.sendMail({from:'donotreply@selectdrive.ru', to:email, subject:sails.__({phrase:"new-booking", locale:params.locale}), html:results.html}, function(err,res){
          if(err){
            return cb({error:err});
          }
          else{
            cb({ok:'Message sent'});
          }
        })
      })





  },

/*
  sendMail:function (email, tpl, subject, url) {
    sails.log.debug(email,url);




    handlebars.registerPartial('link', '{{link.href}}');
    console.log(partnerRegistration);

    var template = new EmailTemplate(tpl),
      locals = {link:{href:url}};

    template.render(locals)
      .then(function(results){
        sails.log.info(results);

        transporter.sendMail({from:'donotreply@selectdrive.ru', to:email, subject:subject, html:results.html}, function(err,res){
          if(err){
            return cb({error:err});
          }
          else{
            cb({ok:'Message sent'});
          }
        })
      })


  },
*/

  sendPartnerActivation: function(email, url, cb){
    sails.log.debug(email,url);


    handlebars.registerPartial('link', '{{link.href}}');
    console.log(partnerRegistration);

    var template = new EmailTemplate(partnerRegistration),
      locals = {link:{href:url}};

    template.render(locals)
      .then(function(results){
        sails.log.info(results);

        transporter.sendMail({from:'donotreply@selectdrive.ru', to:email, subject:'Регистрация партнера на selectdrive.ru', html:results.html}, function(err,res){
          if(err){
            return cb({error:err});
          }
          else{
            cb({ok:'Message sent'});
          }
        })
      })



  }
};
