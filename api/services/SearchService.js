var Promise = require('bluebird');

module.exports = {
// Check whether city param specified in a url query
  getCity: function (id) {

    return new Promise(function(resolve, reject){

      if (id) {
        City.findOne({id:id})
          .then(function (city) {
            resolve(city);
          })

      } else {
        City.findOne({isDefault: true})
          .then(function (city) {
            resolve(city);
          });
      }

    });


  }

};
