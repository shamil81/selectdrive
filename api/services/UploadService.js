/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 08/01/16
 */


var path = require('path'),
    uploadPath = path.resolve('..', 'site/static/uploads'),
    Promise = require('bluebird');


module.exports = {

  imageUpload:function (req, field) {

    return new Promise(function(resolve,reject){
      var allowedTypes = ['image/jpeg', 'image/png', 'image/gif'];

      field = field || 'image';

      console.log('Uploading to: ' + uploadPath);


     // uploaded = req.file(field)._files[0].stream;



      /*   if (_.indexOf(allowedTypes, uploaded.headers['content-type']) === -1) {
       return Promise.reject("Загружайте изображения JPG, PNG, или GIF");
       }*/

      req.file(field).upload({
        dirname: uploadPath,
        maxBytes: 10000000

      }, function whenDone(err, uploadedFiles) {
        if (err) {
          return reject({serverError:err});
        }

        if (uploadedFiles.length === 0) {
          return reject({badRequest:'No file was uploaded'});
        }

        var fileName = uploadedFiles[0].fd,
        imageUrl = sails.config.url.upload + fileName.substr(fileName.lastIndexOf('/'));

        return resolve(imageUrl);

      });
    });
    }




}
