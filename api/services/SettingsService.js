/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 21/10/15
 */



module.exports = {

  ADMIN:{

    ID:0


  },

  /**
   * Min password length
   */
  MIN_PASS_LEN: 6,
  /**
   * Company INN length
   */
  INN_COMPANY_LEN: 10,
  /**
   * Base url
   */
  BASE_URL: 'https://selectdrive.ru/',


  PARTNER_URL: 'https://partner.selectdrive.ru/',

  /**
   * Max "hours" price plan
   */
  MAX_HOUR: 120,

  /**
   * Max "days" price plan
   */
  MAX_DAYS: 90,

  /**
   * Max "months" price plan
   */

  MAX_MONTHS: Number.POSITIVE_INFINITY,

  /**
   * Car image width
   */
  CAR_IMAGE_WIDTH: 360,


  /**
   * Max number of car images allowed
   */

  CAR_IMAGE_MAX_NUM: 5,


  /**
   * User uploads dir
   */


  USER_UPLOAD_DIR: '//static.selectdrive.ru/uploads/',

  /**
   * Local upload path
   */

  UPLOAD_PATH: 'site/static/uploads',

  /**
   *  Default locale
   */
  LOCALE: 'ru',

  USER_TYPE: {
    
    PARTNER:1,
    CLIENT:0
  },
  CAR_STATUS:{

    AVAILABLE:1,
    BOOKED:2,
    PICKEDUP:3,
    NOT_AVAILABLE:4,
    PENDING:99,
    RELEASE:98

  },

  BOOKING_STATUS:{
 
    // Car for booked in a future, but avalailable for now
    PENDING:2,
    // Booking successfully completed
    COMPLETE:3,
    
    CANCELED:4,

    ACTIVE:5,

    PARTNER_CANCELED:6









  },

  MIN_BOOKING_DAYS:2,
  // Time left to car release (Car will be shown in search results if booking time remains less or equal setting below
  RELEASE_HOUR:4,
  MIN_BOOKING_HOURS:24,
  MIN_HOURS_BEFORE_BOOKING:4,
  MAX_BOOKING_HOURS:(function(){return SettingsService.MIN_BOOKING_HOURS * 90}),
  BOOKING_CONFIRMATION_DELAY_HOURS:2,
  

  MIN_RATING_STARS:1,
  MAX_RATING_STARS:5,
// A time delta (in minutes) within which a user has to pay 
  BOOKING_TIME_MINUTES:3,
  // Minimal paiment percentage
  BOOKING_MIN_PAYMENT:30
  //BOOKING_TIME_MS:SettingsService.BOOKING_TIME_MINUTES * 1000 * 60



};
