/**
 * Author: Shamil Bikbov
 * shamil.bikbov@gmail.com
 * Created: 03/12/15
 */

var Promise = require('bluebird');
module.exports = {

  createLink:function(user, cb){
    var hash,
       al;

       ActivationLink.findOne({user:user})
         .then(function (found) {
           if(found){

             return cb(found);

           }else{

             hash = HelperService.randStr();
             al =  {user:user, hash:hash};

             ActivationLink.create(al)
               .then(function(activation){
                 return cb(activation);

               })
               .catch(function(error){
                 sails.log.error(error);
                 cb(error);
               })
           }
         })
         .catch(function(error){
           sails.log.error(error);
           cb(error);
         })



  },

  confirm:function(param){
    var that = this;
    return new Promise(function(resolve, reject){
      ActivationLink.findOne({hash:param.hash})
        .then(function(result){
          if(!result){
            reject({status:404});
          }
          User.update({id:result.user}, {confirmed:true})
            .then(function(user){
              var user = user[0];
              User.findOne({id:user.id})
                .populate('auth')
                .then(function(user){
                  user.email = user.auth.email;
                  delete  user.auth;
                  if(param.hasOwnProperty('password')){
                    console.log('update ' + user.id, param.password);
                    Auth.update({user:user.id}, {password:param.password})
                      .then(function(){
                        that.destroy(param.hash)
                          .then(function(){resolve(user)});
                      })
                  }else{
                    that.destroy(param.hash)
                      .then(function(){resolve(user)});
                  }


                })

            })
        })
        .catch(function(err){
          reject(err);
        })

    });



  },

  get:function(hash){
    return new Promise(function(resolve, reject) {
      ActivationLink.findOne({hash: hash})
        .populate('user')
        .then(function (found) {
         if(found){
           Auth.findOne({id:found.user.auth})
             .then(function(res){
               found.user.email = res.email;
               resolve(found);

             })
         }else{
           resolve(false);
         }

        })
        .catch(function(err){
          reject(err);
        })
    });
  },

  destroy:function(hash) {
    return new Promise(function(resolve, reject) {

      ActivationLink.destroy({hash: hash})
        .then(function (link) {
          resolve(link.id);

        })
        .catch(function(err){
          reject(err);
        })
    });
  },

    hasUser:function(id){

        return new Promise(function(resolve,reject){
            ActivationLink.findOne({user:id})
                .then(function(found){
                    if(found){
                        return resolve(true);
                    }
                    return resolve(false);
                })
                .catch(function(err){
                    reject(err);
                })
        });

    }






}
