module.exports = {

    onError:function(err){

        switch(err.status){

            case 401:
            return 'unauthorized';
            case 400:
            return 'badRequest';
            case 422:
            return 'unprocessableEntity';
            case 402:
            return 'paymentRequired';
            case 404:
            return 'notFound';
            default:
            return 'serverError';
        }





    }


};