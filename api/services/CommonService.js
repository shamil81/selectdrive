/**
 *
 * Contains methods common for models
 */

var Promise = require('bluebird');

module.exports = {
  // Handle models that have isDefault field
  handleDefaults: function (model, value) {

    return new Promise(function (resolve, reject) {
      var Model = sails.models[model.toLowerCase()];
      if (value.isDefault) {
        Model.update({id: {'!': [value.id]}}, {isDefault: false})
          .then(function () {
            return resolve(true);
          })
          .catch(function (err) {
            return reject(err);

          });
      } else {
        Model.find({isDefault:true})
          .then(function (results) {
            if (results.length === 0) {
              value.isDefault = true;
              return resolve(true);
            }
            return resolve(true);
          })
          .catch(function (err) {
            return reject(err);
          });
      }
    });

  },

  // Filter data on Partner's request, otherwise return a whole data set
  filter:function (model,user, query) {
      var Model = sails.models[model.toLowerCase()],
        q = query || {};


      if(user && user.userType === 1){

        q.owner = user.id;

      }

      return Model.find(q);


  },

  // Returns an array of items that have same values as criteriaObj has

  filterByProp:function (ar, criteriaObj) {

    var ranges = ['maxYear', 'minYear', 'maxVol', 'minVol'];

    var filtered = ar.filter(function (item) {

      var fits = true;




      _.forOwn(criteriaObj, function (val, key) {

        var v = parseInt(val,10);


        if(!item[key] && ranges.indexOf(key) > -1){


          switch(key){
            case 'maxYear':
              fits = item.year <= v;
              break;
            case 'minYear':
              fits = item.year >= v;
              break;
            case 'maxVol':
              fits = item.volume <= v;
              break;
            case 'minVol':
              fits = item.volume >= v;
              break;
              default:
              fits = false;
          }

          if(!fits){
            return false;
          }

        }else{

          if (parseInt(item[key]) != v) {

            fits = false;
            return false;

          }

        }



      });

      return fits;

    });


   //console.dir(filtered);

    return filtered;

  }





};
