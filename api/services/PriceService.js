var Promise = require('bluebird');

module.exports = {


  save: function (data) {
    console.dir(data);

    return new Promise(function (resolve, reject) {


      var prices = data.price,
        payload = {title: data.title, owner: data.owner};


      PricePlanType.findOne({id: data.pricePlanType})
        .then(function (pricePlanType) {
          if (pricePlanType) {

            PricePlan.findOne(payload)
              .then(function (exists) {
                if (exists && !data.hasOwnProperty('id')) {
                  return reject({error: "План " + data.title + " уже существует"});
                } else {

                  for (var i in prices) {

                    // Get rid of possible clutter
                    if(Object.keys(prices[i]).length > 3){
                      prices[i] = _.pick(prices[i], ['limited','unlimited', 'discount']);

                    }

                    var key = Number.parseInt(i),
                      runLimited = Number.parseInt(prices[i]['limited']),
                      runUnlimited = Number.parseInt(prices[i]['unlimited']),
                      discount = Number.parseInt(prices[i]['discount']);


                    if (isNaN(key) || key > pricePlanType.max || key < pricePlanType.min || isNaN(runLimited) || runLimited <= 0 || isNaN(runUnlimited) || runUnlimited <= 0 || isNaN(discount) || discount < 0 || discount > 99) {

                      return reject({error: i + ':' + prices[i]});


                    }


                  }




                  if (data.hasOwnProperty('id')) {
                    console.dir(data);


                    PricePlan.update({id: data.id, owner: data.owner}, data)
                      .then(function (found) {
                        console.dir(found);


                        return resolve(found[0]);

                      });

                  } else {

                    PricePlan.create(data)
                      .then(function (plan) {
                        console.log('saved');
                        return resolve(plan);
                      });


                  }
                }
              });


          } else {
            return reject({error: 'Unresolved PricePlanType: ' + data.pricePlanType});
          }

        })


    });

  }


}
