/**
 * RentConditionGroupController
 *
 * @description :: Server-side logic for managing Rentconditiongroups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  create:function (req,res) {

    var data = req.allParams();
    console.dir(data);
    RentConditionGroup.create(data)
      .then(function (results) {
        return res.ok(results);
      })
      .catch(function (err) {
        return res.serverError(err);
      })
  },

  destroy:function(req,res){

    var data = req.allParams();
    RentConditionGroup.destroy({id:data.id, owner:data.owner})
      .then(function(found){
        if(found){
          return res.ok(found);
        }else {
          return res.notFound(data);
        }
      })
      .catch(function(err){
        return res.serverError(err);
      });

  },

  update:function(req,res){
    var data = req.allParams();
    console.dir(data);

    RentConditionGroup.update({id:data.id, owner:data.owner}, data)
      .then(function(found){
        console.dir(found);
        if(found[0]){
          return res.ok(found[0]);
        }else {
          return res.notFound(data);
        }
      })
      .catch(function(err){
        return res.serverError(err);
      });

  },


  find:function(req,res){

    CommonService.filter('RentConditionGroup', req.user, req.allParams())
      .then(function (data) {
        return res.ok(data);
      })
      .catch(function(err){
        return res.serverError(err);
      });







  }


};

