/**
 * LocationController
 *
 * @description :: Server-side logic for managing Locations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
module.exports = {

  create:function(req, res){

    var data = req.allParams();
    console.dir(data);
    LocationService.process(data, req.user.id)
      .then(function(location){
        return res.ok(location);
      })
      .catch(function (err) {
        return res.serverError(err);
      })


  },

  update: function (req, res) {
    var data = req.allParams();

    if(data.owner !== req.user.id){
      return res.unauthorized();
    }

    LocationService.process(data, req.user.id)
      .then(function(location){
        return res.ok(location[0]);
      })
      .catch(function (err) {
        return res.serverError(err);
      })


  },

  destroy: function (req, res) {
    var data = req.allParams();
    Location.findOne({id: data.id, owner: data.owner})
      .then(function (result) {
        if(!result){
          return res.notFound({id:data.id});
        }
        if (result.cars.length > 0) {
          return res.forbidden({error: 'Location in use'})
        }
        Location.destroy({id: data.id, owner: data.owner})
          .then(function (result) {
            return res.ok(result);
          })
          .catch(function (err) {
            return res.serverError(err);
          })
      })


  },

  find:function(req,res){

    CommonService.filter('Location', req.user, req.allParams())
      .populate('cars')
      .then(function (result) {

        return res.ok(_.sortBy(result, ['city']).reverse());
      })
      .catch(function (err) {
        return res.serverError(err);
      });
  }



};

