/**
 * CarController
 *
 * @description :: Server-side logic for managing Cars
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var path = require('path'),
  lwip = require('lwip'),
  uploadPath = path.resolve('..', SettingsService.UPLOAD_PATH),
  fs = require('fs'),
  bcrypt = require('bcrypt-nodejs'),
  moment = require('moment'),
  async = require('async');
  //testData = require('../cars_user_class1.json');


module.exports = {

  search: function (req, res) {

    var params = req.allParams();

    CarService.search(params)
      .then(function (cars) {
        return res.ok(cars);
      })
      .catch(function (err) {

        switch(err.status){
          case 400:
            return res.badRequest(err.error);
          case 422:
          return res.unprocessableEntity(err.error);
          default:
           return res.serverError(err);

        }
        
       
      })

  },
  /**
  * Get car and price
  * 
  * 
  */

  get:function(req,res){

    var id = req.allParams().id;

    CarService.getCar(id)
    .then(res.ok)
    .catch(res.serverError);


  },



  find: function(req,res){

    var p = req.allParams();
    if(p.start && p.end && p.id){

      var valid = BookingService.validate(p);
      
      if(!valid){

        return res.unprocessableEntity();

      }

     p.car = +p.id;


      BookingService.getBookingCar(p)
      .then(res.ok)
      .catch(function(err){

         res[ErrorService.onError(err)](err);


      });





    }

    else {

      res.unprocessableEntity();


    }

 
   

  },



  my: function (req, res) {
    var id = req.user.id;

    Car.find({owner:id})
      .populate('images')
      .then(function (cars) {
        return res.ok(cars);
      })
      .catch(function (err) {
        res.serverError(err);
      })
  },

  update: function (req, res) {
    var data = req.allParams();
    Car.update({id: data.id, owner: req.user.id}, data)
      .then(function (results) {
        return res.ok(results);
      })
      .catch(function (err) {
        res.serverError(err);
      });
  },

  createImage: function (req, res) {
    var data = req.allParams(),
      field = data.field || 'image',
      uploaded = req.file(field)._files[0].stream,
      car = data.car,
      allowedTypes = ['image/jpeg', 'image/png', 'image/gif'];
    console.dir(car);


    if (typeof car === 'undefined' || !car) {
      return res.badRequest('Car id should be specified');
    }

    if (_.indexOf(allowedTypes, uploaded.headers['content-type']) === -1) {
      return res.badRequest("Please upload JPG, PNG, GIF");
    }


    Car.findOne({id: car, owner: req.user.id})
      .populate('images')
      .then(function (found) {
        if (!found) {
          return res.forbidden('You\'re not allowed this car manipulation');
        }


        if (found.images.length === SettingsService.CAR_IMAGE_MAX_NUM) {
          return res.forbidden('You\'ve exceeded the number of images for this car');

        }

        /**
         * TODO:
         * Use upload service bellow
         */

        req.file(field).upload({
          dirname: uploadPath,
          maxBytes: 10000000,

        }, function whenDone(err, uploadedFiles) {
          if (err) {
            return res.serverError(err);
          }

          if (uploadedFiles.length === 0) {
            return res.badRequest('No file was uploaded');
          }


          CarImage.create({
              car: car,
              path: uploadedFiles[0].fd
            })
            .then(function (data) {

              lwip.open(uploadedFiles[0].fd, function (err, image) {
                if (err) {
                  return res.serverError(err);
                }
                var w = image.width(),
                  h = image.height(),
                  ratio = SettingsService.CAR_IMAGE_WIDTH / w;
                image.resize(Number.parseInt(ratio * w, 10), Number.parseInt(ratio * h, 10), function (err) {
                  image.writeFile(uploadedFiles[0].fd, function (err) {
                    if (err) {
                      return res.serverError(err);
                    }
                    return res.ok(data);

                  });

                });

              });


            })
        });


      });
  },

  destroyImage: function (req, res) {

    sails.log.debug(req.allParams());

    var id = req.allParams().id;
    CarImage.findOne({id: id})
      .then(function (image) {
        if(image){
          Car.findOne({id: image.car, owner: req.user.id})
            .then(function (found) {
              if (found) {
                CarImage.destroy({id: id})
                  .then(function () {
                    fs.unlink(image.path, function () {
                      return res.ok({id: image.id});
                    });
                  })

              } else {
                return res.notFound();
              }
            })
            .catch(function (err) {
              return res.serverError(err);
            })
        }else{
          return res.notFound();
        }



      })


  }/*,

  create: function(req,res){
    var arr = testData;
        console.dir(arr);

        async.each(arr, function(car, cb){

           Car.create(car)
           .then(function(created){
            cb();
           })

        }, function(err){
          if(err){
            return res.serverError(err);
          }
          return res.ok('created');
        });

    

  }
*/

};

