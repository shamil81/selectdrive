/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require('passport');
/**
 * Triggers when user authenticates via passport
 * @param {Object} req Request object
 * @param {Object} res Response object
 * @param {Object} error Error object
 * @param {Object} user User profile
 * @param {Object} info Info if some error occurs
 * @private
 */
function _onPassportAuth(req, res, error, user, info) {
  if (error) return res.serverError(error);
  if (!user) {
    if(info.code === 'E_WRONG_PASSWORD'){
      return res.unauthorized(null, info && info.code, info && info.message);
    }else{
      return res.notFound();
    }
  }

  return res.ok({
    // TODO: replace with new type of cipher service
    token: CipherService.createToken(user),
    user: user
  });
}

module.exports = {
  /**
   * Sign up in system
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  /*signup: function (req, res) {

      User.create(_.omit(req.allParams(), 'id'))
      .then(function (user) {
        return {
          token: CipherService.createToken(user),
          user: user
        };
      })
      .then(res.created)
      .catch(res.serverError);
  },
*/
  /**
   * Sign in by local strategy in passport
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  signin: function (req, res) {
    var user = req.allParams();
    if(!user.userType){
      user.userType = 0;
    }
    Auth.findOne({email:user.email, userType:user.userType})
      .then(function(auth){
        if(!auth){
          return res.notFound();
        }
        if (!CipherService.comparePassword(user.password, auth)){
          
          return res.notFound();
        }


        User.findOne({auth:auth.id})
          .then(function(found){
            user.id = found.id;
            found.email = auth.email;
            return res.ok({
              token: CipherService.createToken(user),
              user: found
            });
          });

      })
      .catch(function(err){
        return res.serverError(err);
      })

    //passport.authenticate('local', _onPassportAuth.bind(this, req, res))(req, res);

  },
  reset:function (req, res) {
    var data = req.allParams(),
      email = data.email;
    Auth.findOne({email:email})
      .then(function (found) {
        if(found){
          User.update({id:found.user}, {confirmed:false})
            .then(function () {
              ActivationLinkService.createLink(found.user, function (activation) {


                MailService.sendReminder(found.email, activation.hash)
                  .then(function () {
                    // Discard old password
                    Auth.update({id:found.id}, {password:HelperService.randStr()})
                      .then(function () {
                        return res.ok();
                      })


                  })
                  .catch(function (err) {
                    return res.serverError(err);
                  })

              });
            })

        }else{
          return res.notFound()
        }
      })

  }
};
