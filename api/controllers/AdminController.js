/**
 * AdminController
 *
 * @description :: Server-side logic for managing Admins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  login:function(req,res){
    var user = req.allParams();
    if(user.email !== sails.config.admin.email || user.password !== sails.config.admin.password){

      return res.unauthorized();

    }

    user.id = sails.config.admin.username;
    return res.ok({
      user:{},
      token:CipherService.createToken(user)
  });



  }

};

