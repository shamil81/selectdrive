/**
 * BookingController
 *
 * @description :: Server-side logic for managing Bookings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var schedule = require('node-schedule');

module.exports = {

    create: function(req, res) {

        var params = req.allParams();
        params.user = req.user.id;


        if (!params.id) {

            BookingService.makeIntent(params)
                .then(res.ok)
                .catch(function(err) {

                    res[ErrorService.onError(err)](err);

                });




        } else {


            BookingService.makeBooking(params)
                .then(function(booking) {

                    return res.ok(booking);


                })
                .catch(function(err) {


                    res[ErrorService.onError(err)](err);


                });



        }



    },

    destroy:function(req,res){

      var params = req.allParams();
      params.user = req.user.id;

      console.log('destroy' + params.id);

      BookingService.cancel({user:params.user, id:params.id})
      .then(res.ok)
      .catch(function(err) {


                    res[ErrorService.onError(err)](err);


                });




    },


    setting:function(req,res){

        var city = req.allParams().city;

        BookingService.getSettings(city)
        .then(res.ok)
        .catch(function(err) {

           res[ErrorService.onError(err)](err);

         });


        



   


    }

   

};