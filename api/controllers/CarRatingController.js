/**
 * CarRatingController
 *
 * @description :: Server-side logic for managing Carratings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var isPast = require('date-fns/is_past');

module.exports = {

    create:function(req,res){

      var p = req.allParams();
      


      // Find booking object which is being rated
      Booking.findOne({id:p.booking, car:p.car, user:p.user})
      .then(function(booking) {

          if(!booking){

              return res.notFound();

          }

          // Whether rental time is elapsed
          if(isPast(booking.end)){

              CarRating.create(p)
              .then(res.ok)
              .catch(res.serverError);

          }
          else {
              return res.badRequest({error:'Rate after rental ends'});

          }





      });

    }


	
};

