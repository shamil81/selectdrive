/**
 * CustomerRequirementsController
 *
 * @description :: Server-side logic for managing Customerrequirements
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


  update: function (req, res) {
    var data = req.allParams();
    console.dir(data);
    CustomerRequirement.update({id: data.id, owner: data.owner}, data)
      .then(function (result) {
        console.dir(result);
        return res.ok(result[0]);
      })
      .catch(function (err) {
        return res.serverError(err);
      });
  },

  destroy: function (req, res) {
    var data = req.allParams();

    CustomerRequirement.destroy({id: data.id, owner: data.owner})
      .then(function (result) {
        return res.ok(result);
      })
      .catch(function (err) {
        return res.serverError(err);
      });


  },

  find:function(req,res){




    CommonService.filter('CustomerRequirement', req.user, req.allParams())
      .then(function (result) {
        return res.ok(result);
      })
      .catch(function (err) {
        return res.serverError(err);
      });
  }


};

