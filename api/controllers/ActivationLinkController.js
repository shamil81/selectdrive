/**
 * ActivationlinkController
 *
 * @description :: Server-side logic for managing activationlinks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  confirm:function(req,res){
    var param = req.allParams();
    ActivationLinkService.confirm(param)
      .then(function(user){
        res.ok(user);
      })
      .catch(function(err){
        if(err.status === 404){
          return res.notFound();
        }
        res.serverError(err);
      })

  },


  get:function(req,res){
    var param = req.allParams();
    console.dir(param);
    ActivationLinkService.get(param.hash)
      .then(function(result){

        return res.ok(result);
      })
      .catch(function(err){
        res.serverError(err);
      })
  }
};

