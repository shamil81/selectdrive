/**
 * CityController
 *
 * @description :: Server-side logic for managing Cities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  find:function(req,res){
    var locale = req.allParams().locale || SettingsService.LOCALE;
    City.find()
      .then(function(results){
        var cities = _.sortBy(results, function(o){
          return o.title[locale];
        });
        return res.ok(cities);

      })
      .catch(function(err){
        return res.serverError(err);
      });
  }

};

