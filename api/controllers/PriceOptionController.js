/**
 * PriceOptionController
 *
 * @description :: Server-side logic for managing Priceoptions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var cloudinary = require('cloudinary');
cloudinary.config ({
  cloud_name:'dadu4ldgb',
  api_key:'257558956287972',
  api_secret:'KNfR5hlyRL5kTzkTbG_twdTIk8c'
});
module.exports = {

  upload:function(req,res){

    var data = req.allParams(),
      isUpdate = false,
      userId = 0;
    console.log('title');
    console.dir(data);


    PriceOption.findOne({id: data.id})
      .then(function (found) {
        if (found) {
          isUpdate = true;
        }

        //console.log(sails.config.url.appPath);
        req.file('file').upload({
            dirname: sails.config.url.appPath,
            maxBytes: 10000000
          },
          function whenDone(err, uploadedFiles) {
            if (err) {
              console.log('err');
              return res.serverError(err);
            }


            console.log('file ' + uploadedFiles.length);


            //Unless new image
            if (uploadedFiles.length === 0 || req.user.id !==  sails.config.admin.id) {
              if (isUpdate) {
                PriceOption.update({id: data.id},data)

                  .then(function (o) {
                    return res.ok(o);
                  })
              }else{
                PriceOption.create({
                    title: {ru: data.title.ru, en: data.title.en},
                    description: {ru: data.description.ru, en: data.description.en},
                    createdBy:userId
                  })
                  .then(function (maker) {
                    return res.ok(maker);
                  })
                  .catch(function (err) {
                    return res.serverError(err);
                  })
              }
            } else {


              cloudinary.uploader.upload(uploadedFiles[0].fd, function (result) {

                if (isUpdate) {
                  console.log('update');
                  PriceOption.update({id:data.id},
                  {
                      //title: {ru: data.title.ru, en: data.title.en},
                      image: result.secure_url
                     // description: {ru: data.description.ru, en: data.description.en}
                    })
                    .then(function (o) {
                      return res.ok(o[0]);
                    })
                    .catch(function (err) {
                      return res.serverError(err);
                    })
                } else {
                  PriceOption.create({
                      //title: {ru: data.title.ru, en: data.title.en},
                      image: result.secure_url
                      //description: {ru: data.description.ru, en: data.description.en}
                    })
                    .then(function (o) {
                      return res.ok(o);
                    })
                    .catch(function (err) {
                      return res.serverError(err);
                    })
                }


              });


            }


          });
      })


  },
  get:function(req,res){
    console.dir(req.user);
    var userId = req.user.id;
    console.log(userId);
    PriceOption.find({createdBy:[sails.config.admin.id, userId]})
      .then(function(result){
        return res.ok(result);
      })
      .catch(function(err){
        return res.serverError(err);

      });
  },

  destroy:function(req,res){
    var data = req.allParams(),
      option = null;
    data.owner = req.user.id;
    PriceOption.destroy({id:data.id, createdBy:data.owner})
      .then(function(result){
        // Destroy assoc data
        PriceOptionItem.destroy({priceOption:data.id})
          .then(function(items){

            option = result;
            option.priceOptionItem = items;
            return res.ok(option);


          })
      })
      .catch(function(err){
        return res.serverError(err);

      });
  }

};


