/**
 * PricePlanController
 *
 * @description :: Server-side logic for managing Priceplans
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  create:function(req,res){
    var data = req.allParams();
    data.owner = req.user.id;
    PriceService.save(data).then(function(result){
      return res.ok(result);


    })
      .catch(function(err){
      return res.serverError(err);
    });
  },
  // find:function(req,res){

  //   CommonService.filter('PricePlan', req.user, req.allParams())
  //     .then(function (result) {
  //       return res.ok(_.sortBy(result, ['id']).reverse());
  //     })
  //     .catch(function (err) {
  //       return res.serverError(err);
  //     });
  // },


  update:function (req,res) {

    var data = req.allParams();

    PricePlan.findOne({id:data.id, owner:req.user.id})
      .then(function (found) {
        if(found){
          PricePlan.update({id:data.id}, data)
            .then(function (plan) {
              return res.ok(plan);
            });
        }else{
          return res.forbidden();
        }
      })


  }


};

