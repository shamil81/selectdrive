/**
 * ClientController
 *
 * @description :: Server-side logic for managing Clients
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var validator = require('validator');


module.exports = {

  /**
   * Creates a new user of type Client
   * @param req
   * @param res
   */

  signup: function (req, res) {
    var user = req.allParams();


    if (user) {

      console.dir(user);
      req.setLocale(user.locale);

      Auth.findOne({
        or: [
          { email: user.email }


        ]
      })
        .then(function (found) {
          if (found) {
            return res.conflict(req.__("user-already-exists", found.email));
          } else {


            
         



            Auth.create({ email: user.email, password: user.password, userType: 0 })
              .then(function (auth) {
                user.auth = auth.id;
                user.birthDate = new Date(user.birthDate);
                user.driverLicenseDate = new Date(user.driverLicenseDate);
                user.passportDate = new Date(user.passportDate);
                user.confirmed = false;

                User.create(user)
                  .then(function (created) {



                    user.id = created.id;

                    Auth.update(created.auth, { user: user.id })
                      .then(function (auth) {
                        ActivationLinkService.createLink(user.id, function (activation) {
                          console.dir(activation);
                          MailService.sendActivation(user.email, activation.hash, user.locale, function () {
                            return res.ok({
                              token: CipherService.createToken(user),
                              user: user
                            });

                          });

                        })

                      })


                  })
                  .catch(function(err){
                    Auth.destroy({id:auth.id})
                    .then(res.serverError(err));
                  })
              })

              .catch(res.serverError);

          }
        })



    } else {
      return res.badRequest();
    }


  },

  confirm: function (req, res) {
    var key = req.param('key');
    if (key) {
      ActivationLink.destroy({ hash: key })
        .then(function (activation) {
          if (!activation[0]) {
            return res.notFound();
          }
          User.update({ id: activation[0].user }, { confirmed: true })
            .then(function (user) {
              return res.ok('confirmed');

            })
        })
        .catch(function (err) {
          return res.serverError(err);
        })
    } else {
      return res.badRequest();
    }

  },


  find: function (req, res) {
    var id = req.user.id;
    User.findOne({ id: id })
      .populate('auth')
      .populate('locations')
      .then(function (user) {
        user.email = user.auth.email;
        delete user.auth;
        if (user.isLegalEntity) {
          User.find({ createdBy: user.id })
            .then(function (drivers) {
              user.drivers = drivers;
              Partner.findOne({ user: id })

                .then(function (partner) {

                  if(partner){
                    
                  delete partner.id;
                
                  _.merge(user, partner);

                  }

                  
                  
                  return res.ok(user);


                })
            })
        } else {
          return res.ok(user);
        }
      })
      .catch(function (err) {
        return res.serverError(err);
      })


  },

  addDrivers: function (req, res) {
    var drivers = req.param('drivers'),
      userId = req.user.id;
    User.findOne({ id: userId })
      .then(function (user) {
        if (user.isLegalEntity) {
          drivers.forEach(function (driver) {
            driver.owner = userId;
          })
          User.create(drivers)
            .then(function (drivers) {
              res.ok(drivers);
            });
        } else {
          res.forbidden('You\'re not allowed to submit drivers');
        }
      })

  },



  update: function (req, res) {
    var user = req.allParams();
    user.id = req.user.id;
    UserService.update(user)
      .then(function (result) {
        if (result === 'unauthorized') {
          res.unauthorized();
        } else if (result === 'conflict') {
          res.conflict();
        } else {
          res.ok(result);
        }
      })
  }





};

