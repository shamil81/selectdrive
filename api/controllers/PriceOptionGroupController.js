/**
 * PriceOptionGroupController
 *
 * @description :: Server-side logic for managing Priceoptiongroups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	get:function(req,res){
    PriceOptionGroup.find({owner:req.user.id})
      .populate('options')
      .then(function(groups){
        return res.ok(groups);
      })
      .catch(function(err){
        return res.serverError(err);
      })
  },

  destroy:function(req,res){

    var data = req.allParams();

    console.dir(data);


    PriceOptionGroup.destroy({id:data.id, owner:data.owner})
      .then(function(group){
        PriceOptionItem.destroy({group:group[0].id})
          .then(function(result){
            group.options = result;
            return res.ok(group);
          });


      })
      .catch(function(err){
      return res.serverError(err);
    })



  },

  create:function(req,res){
    var data = req.allParams();
    console.dir(data);

    PriceOptionGroup.create(data)
      .then(function(group){
        PriceOptionGroup.findOne({id:group.id})
          .populate('options')
          .then(function(groups){
            return res.ok(groups);

          });


      })
      .catch(function(err){
        return res.serverError(err);
      })
  },

  update:function(req, res){
    var data = req.allParams();
    console.dir(data);


    if(data.isDefault){
      PriceOptionGroup.update({isDefault:true}, {isDefault:false})
        .then(function(){
          PriceOptionService.updateGroup(data)
            .then(function(o){
              return res.ok(o);
            })
        })
        .catch(function(err){
          return res.serverError(err);
        });

    }else{
      PriceOptionService.updateGroup(data)
        .then(function(o){
          return res.ok(o);
        })
    }


  }
};

