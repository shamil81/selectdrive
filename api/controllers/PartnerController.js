/**
 * PartnerController
 *
 * @description :: Server-side logic for managing Partners
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
// Shold be refactored
  create:function(req,res){
    console.log('part');

    var partner = req.allParams(),
      partnerLegal = ['head', 'bank', 'bik', 'k_account', 'r_account'],
      payload = {},
      hash = '',
      url = '';


if(partner.isLegalEntity){
  partnerLegal.map(function(el){
    payload[el] = partner[el];
  })

  Partner.findOne({or:[
    {bik:partner.bik},
    {k_account:partner.k_account},
    {r_account:partner.r_account}
  ]})
    .then(function(found){
      if(found){
        console.dir(found);
        return res.conflict({unique:found});
      }else{

           User.findOne({or:[
            {phone:partner.phone},
            {legalEntityInn:partner.legalEntityInn},
            {legalEntityKpp:partner.legalEntityKpp}
            ]})
            .then(function(usr){

              if(found){

                return res.conflict({unique:found});


              }

               Auth.findOne({email:partner.email})
          .then(function(found){
            if(found){
              return res.conflict('Email');
            }
            User.create(partner)
              .then(function(user){
                Auth.create({email:partner.email, password:HelperService.randStr(), userType:1, user:user.id})
                  .then(function(auth){

                      payload.user = user.id;
                      Partner.create(payload)
                        .then(function(partner){
                          User.update({id:user.id}, {partner:partner.id, auth:auth.id})
                            .then(function(){
                                hash = HelperService.randStr(),
                                  url = sails.config.url.partner + '/confirm/' + hash;
                                ActivationLink.create({user:user.id, hash:hash})
                                  .then(function(link){
                                    MailService.sendPartnerActivation(auth.email,url,function(result){

                                      sails.log.info(result);
                                      return res.ok(_.merge(user, partner));
                                  })



                                })
                                .catch(res.serverError);

                              
                              

                            })
                            .catch(res.serverError);
                        })
                        .catch(function(err){
                          return res.serverError(err);
                        })


                  })
                  .catch(res.serverError);

              })
              .catch(function(err){
                return res.serverError(err);
              })

          })

              




            })
            .catch(res.badRequest);


       
      }
    })
    .catch(res.serverError);
}else{
  Auth.findOne({email:partner.email})
    .then(function(found){
      if(found){
        return res.conflict();
      }
     partner = {email:partner.email,
         address:partner.address,
         birthDate:partner.birthDate,
         firstName:partner.firstName,
         lastName:partner.lastName,
         isLegalEntity:partner.isLegalEntity,
         passportDate:partner.passportDate,
         passportIssuedBy:partner.passportIssuedBy,
         passportNumber:partner.passportNumber,
         phone:partner.phone,
         partner:null

     };

     User.findOne({or:[{passportNumber:partner.passportNumber}, {phone:partner.phone}]})
     .then(function(usr){



       if(user){

         return res.conflict();


       }

       User.create(partner)
        .then(function(user){
          Auth.create({email:partner.email, password:HelperService.randStr(), userType:1, user:user.id})
            .then(function(auth){

                console.log('auth');


                User.update({id:user.id}, {auth:auth.id})
                  .then(function(){
                      console.log('finsish');
                    try {
                      hash = HelperService.randStr(),
                        url = sails.config.url.partner + '/confirm/' + hash;
                      ActivationLink.create({user:user.id, hash:hash})
                        .then(function(link){
                          MailService.sendPartnerActivation(auth.email,url,function(result){

                            sails.log.info(result);
                            return res.ok(user);
                          })



                        });

                    }
                    catch(e){
                      sails.log.error(e);
                      res.serverError(e);
                    }
                  })



            })

        })
        .catch(function(err){
          return res.serverError(err);
        })


     })
     .catch(res.serverError);

      

    })
}





  },




    update:function(req,res){
        var data = req.allParams();
        console.dir(data);

        Auth.findOne({user:data.uid})
            .then(function(result){
                console.dir(result);
                if(data.email !== result.email){

                    Auth.findOne({email:data.email})
                        .then(function(email){
                            if(email){
                                return res.conflict("User with " + data.email + " already exists");
                            }

                            var hash = HelperService.randStr(),
                                url = sails.config.url.partner + '/confirm/' + hash;
                            ActivationLink.findOne({user:data.uid})
                                .then(function(found){
                                    if(found){

                                        MailService.sendPartnerActivation(data.email,url,function(result){
                                            Auth.update({user:data.uid}, {email:data.email})
                                                .then(function(result){

                                                    User.update({id:data.id}, data)
                                                        .then(function(result){
                                                            Partner.update({user:data.id}, data)
                                                                .then(function(result){
                                                                    data.confirmed = false;


                                                                    return res.ok(result);
                                                                })
                                                        })
                                                        .catch(function(err){
                                                            return res.serverError(err);
                                                        })
                                                })

                                        })



                                    }else{
                                        ActivationLink.create({user:data.id, hash:hash})
                                            .then(function(link){
                                                MailService.sendPartnerActivation(data.email,url,function(result){
                                                    Auth.update({user:data.id}, {email:data.email})
                                                        .then(function(result){

                                                            User.update({id:data.id}, data)
                                                                .then(function(result){
                                                                    Partner.update({id:data.uid}, data)
                                                                        .then(function(result){
                                                                            data.confirmed = false;


                                                                            return res.ok(result);
                                                                        })
                                                                })
                                                                .catch(function(err){
                                                                    return res.serverError(err);
                                                                })
                                                        })

                                                })

                                            })
                                    }
                                })

                        })

                }else{
                    Partner.update({user:data.id}, data)
                        .then(function(result){
                            User.update({id:data.id}, data)
                                .then(function(result){

                                    return res.ok(result);
                                })
                        })
                        .catch(function(err){
                            return res.serverError(err);
                        })
                }
            })

    }



};

