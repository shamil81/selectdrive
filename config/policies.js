/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {


  // '*': ['isAuthenticated'],

  AuthController: {
    '*': true
  },
  UserController: {
    '*': true,
    'find': ['isAuthenticated'],
    'update': ['isAuthenticated'],
    'destroy':['isAdmin']

  },

  ClientController: {
    'signup': ['isPublic']
  },
  PartnerController: {
    'create': ['isAdmin'],
    'destroy':['isAdmin']

  },
  ActivationLinkController: {
    '*': true
  },

  CarMakerController: {
    'create': ['isAdmin'],
    'update': ['isAdmin'],
    'destroy':['isAdmin']

  },

  UploadController: {
    '*': ['isAdmin']
  },
  CarBodyStyleController: {
    'create': ['isAdmin'],
    'update': ['isAdmin'],
    'destroy': ['isAdmin']
  },

  CarModelController: {
    'test': true,
    'upload': ['isAdmin'],
    'create': ['isAdmin'],
    'update': ['isAdmin'],
    'destroy': ['isAdmin']

  },

  CarColorController: {
    'create': ['isAdmin'],
    'update': ['isAdmin', function (req, res, next) {
      if (req.body.hasOwnProperty('car')) {
        delete req.body.car;

      }
      next();
    }],
    'destroy': ['isAdmin']
  },

  FuelTypeController: {
    'create': ['isAdmin'],
    'update': ['isAdmin'],
    'destroy': ['isAdmin']
  },
  CarController: {
    'create': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],
    'my':['isPartner'],
    'update': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }],
    'createImage': ['isPartner'],
    'destroyImage': ['isPartner'],
    'destroy':['isAdmin']

  },

  CarStatusController: {
    'create': ['isAdmin'],
    'update': ['isAdmin'],
    'destroy': ['isAdmin']
  },

  EngineVolumeController: {
    'create': ['isAdmin'],
    'update': ['isAdmin'],
    'destroy': ['isAdmin']
  },

  WheeldriveController: {
    'create': ['isAdmin'],
    'update': ['isAdmin'],
    'destroy': ['isAdmin']

  },

  PartnerController: {
    'create': ['isAdmin'],
    'update': ['isAdmin'],
    'destroy': ['isAdmin'],
    'find':['isAdmin']
  },

  PricePlanTypeController: {
    'create': ['isAdmin'],
    'update': ['isAdmin'],
    'destroy': ['isAdmin']
  },

  PricePlanController: {
    'create': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],

    'update': ['isPartner'],

    'destroy': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ]
  },

  RentConditionGroupController: {
    'create': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],
    'update': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],

    'destroy': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],

    'find':['isPublic']


  },

  LocationController: {
    'create': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],
    'update': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],

    'destroy': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],
    'find':['isPublic']

  },

  CustomerRequirementController: {
    'create': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],
    'update': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],

    'destroy': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],

    'find':['isPublic']
  },
  PriceOptionController: {
    'upload': ['isAuthenticated'],

    'update': ['isAuthenticated'],
    'get': ['isAuthenticated'],

    'destroy': ['isAuthenticated', function (req, res, next) {
      req.body.owner = req.user.id;
      next();
    }],
    'create': ['isAuthenticated', function (req, res, next) {
      req.body.createdBy = req.user.id;
      next();
    }]
  },
  PriceOptionGroupController: {
    'get': ['isPartner'],
    'create': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],
    'destroy': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }],
    'update': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }

    ]


  },
  PriceOptionItem: {
    'destroy': ['isPartner', function (req, res, next) {
      req.body.owner = req.user.id;
      next();
    }]

  },

  CarImageController: {
    'create': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],
    'update': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ],

    'destroy': ['isPartner',
      function (req, res, next) {
        req.body.owner = req.user.id;
        next();
      }
    ]


  },

  CityController: {
    'create': ['isAdmin'],
    'update': ['isAdmin'],
    'destroy': ['isAdmin']
  },


  CarRatingController: {

    'create':['isAuthenticated'],
    'destroy':['isAdmin'],
    'update':['isAdmin']


  },

  BookingController: {

    'create':['isAuthenticated'],
    'update':['isAdmin'],
    'destroy':['isAuthenticated']



  }


};
