/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {
  '/*' : 'SessionController.disable',
  'post /user' : 'UserController.signup',
  'get /user' : 'UserController.find',
  'post /auth' : 'AuthController.signin',
  'put /user':'UserController.update',
  'get /search': 'CarController.search',


  'post /admin' : 'AdminController.login',

  'post /partner' : 'PartnerController.create',

  'put /partner/:uid':'PartnerController.update',
  'post /confirm':'ActivationLinkController.confirm',
  'get /confirm':'ActivationLinkController.get',
  'put /rentconditiongroup':'RentConditionGroup.update',
  'put /location':'LocationController.update',
  'delete /location':'LocationController.destroy',
  'put /customerrequirement':'CustomerRequirement.update',
  'delete /customerrequirement':'CustomerRequirement.destroy',

  'put /priceoption':'PriceOption.upload',

  'put /priceoptiongroup':'PriceOptionGroup.update',

  'put /car':'CarController.update',
  'post /car/image':'CarController.createImage',

  'get /car/image/:id':'CarImageController.find',
  'delete /car/image':'CarController.destroyImage'




  // 'post /carmaker':'CarMakerController.create',
 // 'get /carmaker':'CarMakerController.find'




};
