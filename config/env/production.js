/**
 * Production environment settings
 *
 * This file can include shared settings for a production environment,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the production        *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  models: {
    connection: 'production'
  },




  /***************************************************************************
   * Set the port in the production environment to 80                        *
   ***************************************************************************/

  // port: 80,

  /***************************************************************************
   * Set the log level in production environment to "silent"                 *
   ***************************************************************************/

  log: {
     level: "silent"
   },
  url:{
    partner:'http://partner.selectdrive.ru',
    client:'http://selectdrive.ru',
    appPath:'/usr/share/nginx/html/site/static/uploads',
    cdn:'http://static.selectdrive.ru',
    upload:'http://static.selectdrive.ru/uploads'

  },
  admin:{
    id:0,
    username:0,
    email:'a@a.ru',
    password:'123456'
  },

  maxUpload:10000000



};
