/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  models: {
    connection: 'default'
  },
  port:1337,
  url:{
    partner:'http://localhost:8081',
    client:'http://localhost:8080',
    appPath:'/Users/macbook/WebstormProjects/sd-dashboard/build/assets/images/uploads',
    upload:'http://localhost:8084/uploads'


  },
  admin:{
    id:0,
    username:0,
    email:'a@a.ru',
    password:'123456'
  },
  maxUpload:10000000



};
